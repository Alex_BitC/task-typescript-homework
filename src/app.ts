import IMoviDOM from './models/iMoviDOM';
import { ApiService } from './services/apiService';
import { DOMService } from './services/domService';

class App {
  private apiService: ApiService;
  private domService: DOMService;
  private moviesList: IMoviDOM[];
  private favoriteMoviesList: IMoviDOM[];
  private favoriteMovieIdList: number[];
  private lastLoad = '';
  private lastquery = '';
  private page = 1;

  constructor() {
    this.apiService = new ApiService();
    this.domService = new DOMService();
    this.moviesList = [];
    this.favoriteMoviesList = [];
    this.favoriteMovieIdList = [];

    this.getLocalStorageFavoriteMovieIdList();
    this.startTrackingLoadMoreButton.bind(this);
    this.loadPopularMovies.bind(this);
  }

  start(): void {
    this.saveAndLoadFavoriteMovieIdList();
    this.apiService.getPopularMovies();
    this.loadPopularMovies();
    this.startTrackingUpcomingButton();
    this.startTrackingPopularButton();
    this.startTrackingTopRatedButton();
    this.startTrackingSubmitButton();
    this.startTrackingLoadMoreButton();
    this.trackFavorite();
  }

  async loadPopularMovies(page = 1): Promise<void> {
    this.lastLoad = 'popular';
    this.moviesList = await this.apiService.getPopularMovies(page);
    this.page = 1;
    this.render();
  }

  startTrackingUpcomingButton(): void {
    const BUTTON_UPCOMING = document.querySelector('#upcoming') as HTMLInputElement;
    BUTTON_UPCOMING!.addEventListener('click', async () => {
      if (BUTTON_UPCOMING!.checked) {
        this.moviesList = await this.apiService.getUpcomingMovies();
        this.lastLoad = 'upcoming';
        this.page = 1;
        this.render();
      }
    });
  }

  startTrackingPopularButton(): void {
    const BUTTON_POPULAR = document.querySelector('#popular') as HTMLInputElement;
    BUTTON_POPULAR!.addEventListener('click', async () => {
      if (BUTTON_POPULAR!.checked) {
        this.moviesList = await this.apiService.getPopularMovies();
        this.lastLoad = 'popular';
        this.page = 1;
        this.render();
      }
    });
  }

  startTrackingTopRatedButton(): void {
    const BUTTON_TOP_RATED = document.querySelector('#top_rated') as HTMLInputElement;
    BUTTON_TOP_RATED!.addEventListener('click', async () => {
      if (BUTTON_TOP_RATED!.checked) {
        this.moviesList = await this.apiService.getTopRatedMovies();
        this.lastLoad = 'top_rated';
        this.page = 1;
        this.render();
      }
    });
  }

  startTrackingSubmitButton(): void {
    const BUTTON_SUBMIT = document.querySelector('#submit') as HTMLButtonElement;
    const INPUT_SEARCH = document.querySelector('#search') as HTMLInputElement;
    BUTTON_SUBMIT!.addEventListener('click', async () => {
        this.lastquery = INPUT_SEARCH.value;
        this.moviesList = await this.apiService.searchMovies(this.lastquery);
        this.lastLoad = 'search';
        this.page = 1;
        this.render();
    });
  }

  startTrackingLoadMoreButton(): void {
    const BUTTON_LOAD_MORE = document.querySelector('#load-more') as HTMLButtonElement;
    BUTTON_LOAD_MORE!.addEventListener('click', async () => {
      const INPUT_SEARCH = document.querySelector('#search') as HTMLInputElement;
      switch (this.lastLoad) {
        case 'popular':
          this.page += 1;
          const moviesPopular = await this.apiService.getPopularMovies(this.page);
          console.log(moviesPopular);
          this.moviesList = this.moviesList.concat(moviesPopular);
          this.lastLoad = 'popular';
          this.render();
          break;
        case 'upcoming':
          this.page += 1;
          const moviesUpComing = await this.apiService.getUpcomingMovies(this.page);
          this.moviesList = this.moviesList.concat(moviesUpComing);
          this.lastLoad = 'upcoming';
          this.render();
          break;
        case 'top_rated':
          this.page += 1;
          const moviesTopRated = await this.apiService.getTopRatedMovies(this.page);
          this.moviesList = this.moviesList.concat(moviesTopRated);
          this.lastLoad = 'top_rated';
          this.render();
          break;
        case 'search':
          if (this.lastquery === INPUT_SEARCH.value) {
            this.page += 1;
            const moviesSearch = await this.apiService.searchMovies(this.lastquery, this.page);
            this.moviesList = this.moviesList.concat(moviesSearch);
          } else {
            this.moviesList = await this.apiService.searchMovies(this.lastquery);
          }
          this.lastLoad = 'search';
          this.render();
          break;
        default:
          break;
      }
    });
  }

  render(): void {
    this.renderRandomMovie();
    this.renderMoviesList();
    this.renderFavoriteMovies();
  }

  renderMoviesList(): void {
    const FILM_CONTAINER = document.querySelector('#film-container');
    FILM_CONTAINER!.innerHTML = '';
    this.moviesList.forEach(movieData => {
      if (this.favoriteMovieIdList.includes(movieData.id)) {
        FILM_CONTAINER!.innerHTML += this.domService.createMovieLikesCard(movieData);
      } else {
        FILM_CONTAINER!.innerHTML += this.domService.createMovieCard(movieData);
      }
    });
  }

  renderRandomMovie(): void {
    const RANDOM_MOVIE_BLOCK = document.querySelector('#random-movie');
    const randomMovie = this.getRandomMovie();
    RANDOM_MOVIE_BLOCK!.innerHTML = this.domService.createRandomMovieBlock(randomMovie);
  }

  getRandomMovie(): IMoviDOM {
    const randomIndex = Math.floor(Math.random() * this.moviesList.length);
    return this.moviesList[randomIndex];
  }


  trackFavorite(): void {
    const FILM_CONTAINER = document.querySelector('#film-container');
    FILM_CONTAINER!.addEventListener('click', (event) => {
      const cardList = Array.from(document.querySelectorAll('.movie-card'));
      const eventTarget = event.target as HTMLElement;
      if (cardList.includes(eventTarget.closest('.movie-card') as  HTMLElement)) {
        const index = cardList.indexOf(eventTarget.closest('.movie-card') as  HTMLElement);
        const idLikesFilm = this.moviesList[index].id;
        if (!this.favoriteMovieIdList.includes(idLikesFilm)) {
          this.favoriteMovieIdList.push(idLikesFilm);
        } else {
          const deleteIndex = this.favoriteMovieIdList.indexOf(idLikesFilm);
          this.favoriteMovieIdList.splice(deleteIndex, 1);
        }
        this.loadFavoriteMovies();
        this.render();
      }
    });
  }

  async loadFavoriteMovies(): Promise<void> {
    this.favoriteMoviesList = [];
    this.favoriteMovieIdList.forEach(async id => {
      const favoriteMovieData = await this.apiService.getMovieById(id);
      this.favoriteMoviesList.push(favoriteMovieData);
    });
  }

  renderFavoriteMovies(): void {
    console.log('render')
    const FAVORITE_MOVIES_BLOCK = document.querySelector('#favorite-movies');
    FAVORITE_MOVIES_BLOCK!.innerHTML = '';
    console.log(this.favoriteMoviesList);
    console.log(this.moviesList)
    this.favoriteMoviesList.forEach(favoriteMovie => {
      console.log(this.domService.createFavoriteMovieCard(favoriteMovie))
      FAVORITE_MOVIES_BLOCK!.innerHTML += this.domService.createFavoriteMovieCard(favoriteMovie);
    });
    this.favoriteMoviesList.forEach(favoriteMovie => {
      console.log('begin')
      const str = this.domService.createFavoriteMovieCard(favoriteMovie);
      console.log(str)
    })

  }

  setLocalStorageFavoriteMovieIdList = () => {
    const savedValue = JSON.stringify(this.favoriteMovieIdList);
    localStorage.setItem('favoriteMovieIdList', savedValue);
  }

  getLocalStorageFavoriteMovieIdList = () => {
    if (!localStorage.getItem('favoriteMovieIdList')) {
      this.favoriteMovieIdList = [];
    } else {
      const savedValue = localStorage.getItem('favoriteMovieIdList');
      if (!savedValue) {
        this.favoriteMovieIdList = [];
      } else {
        this.favoriteMovieIdList = JSON.parse(savedValue);
      }
    }
  }

  saveAndLoadFavoriteMovieIdList() {
    window.addEventListener('beforeunload', this.setLocalStorageFavoriteMovieIdList);
    window.addEventListener('load', this.getLocalStorageFavoriteMovieIdList);
  }
}

export default App;

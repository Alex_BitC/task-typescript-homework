import IResponseAPI from '../models/iResponseApi';
import IResponseMovi from '../models/iResponseMovi';
import { converToMoviDOM } from '../helpers/convertToMovDOMi';
import IMoviDetals from '../models/iMoviDetals';

export class ApiService {
  private readonly apiKey = "2d5c0a11b1a8ab46f188e00c1c5c5eac";

  async searchMovies(query: string, page = 1) {
    try {
      const url = `https://api.themoviedb.org/3/search/movie?api_key=${this.apiKey}&language=en-US&query=${query}&page=${page}`;
      const apiResult: IResponseAPI = await this.load(url);
      const movies: IResponseMovi[] = apiResult.results;
      return movies.map(converToMoviDOM);
    } catch (error) {
      throw error;
    }
  }

  async getPopularMovies(page = 1) {
    try {
      const url = `https://api.themoviedb.org/3/movie/popular?api_key=${this.apiKey}&language=en-US&page=${page}`;
      const apiResult: IResponseAPI = await this.load(url);
      const movies: IResponseMovi[] = apiResult.results;
      return movies.map(converToMoviDOM);
    } catch (error) {
      throw error;
    }
  }

  async getTopRatedMovies(page = 1) {
    try {
      const url = `https://api.themoviedb.org/3/movie/top_rated?api_key=${this.apiKey}&language=en-US&page=${page}`;
      const apiResult: IResponseAPI = await this.load(url);
      const movies: IResponseMovi[] = apiResult.results;
      return movies.map(converToMoviDOM);
    } catch (error) {
      throw error;
    }
  }

  async getUpcomingMovies(page = 1) {
    try {
      const url = `https://api.themoviedb.org/3/movie/upcoming?api_key=${this.apiKey}&language=en-US&page=${page}`;
      const apiResult: IResponseAPI = await this.load(url);
      const movies: IResponseMovi[] = apiResult.results;
      return movies.map(converToMoviDOM);
    } catch (error) {
      throw error;
    }
  }

  async getMovieById(id: number) {
    try {
      const url = `https://api.themoviedb.org/3/movie/${id}?api_key=${this.apiKey}&language=en-US`;
      const apiResult: IMoviDetals = await this.load(url);
      return converToMoviDOM(apiResult);
    } catch (error) {
      throw error;
    }
  }

  errorHandler(res: Response) {
    if (!res.ok) {
        if (res.status === 401 || res.status === 404)
            console.log(`Sorry, but there is ${res.status} error: ${res.statusText}`);
        throw Error(res.statusText);
    }
    return res;
  }

  async load(url: string) {
    return fetch(url, { method: 'GET'})
          .then(this.errorHandler)
          .then((res) => res.json())
          .catch((error) => {
            throw error;
          });
  }
}
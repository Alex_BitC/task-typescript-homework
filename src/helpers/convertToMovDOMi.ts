import IResponseMovi from '../models/iResponseMovi';
import IMoviDOM from '../models/iMoviDOM';
import IMoviDetals from '../models/iMoviDetals';

export function converToMoviDOM(responseMovi: IResponseMovi | IMoviDetals): IMoviDOM {
  return {
    poster_path: responseMovi.poster_path,
    overview: responseMovi.overview,
    release_date: responseMovi.release_date,
    id: responseMovi.id,
    title: responseMovi.title
  };
}
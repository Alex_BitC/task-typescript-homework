import IGendre from './iMoviDetals/iGendre';
import IProductionCompany from './iMoviDetals/iProductionCompany';
import IProductionCountry from './iMoviDetals/iProductionCountry';
import ISpokenLanguage from './iMoviDetals/iSpokenLanguage';

interface IMoviDetals {
  adult: boolean,
  backdrop_path: string | null,
  belongs_to_collection: null,
  budget: number,
  genres: IGendre[],
  homepage: string | null,
  id: number,
  imdb_id: string | null,
  original_language: string,
  original_title: string,
  overview: string | null,
  popularity: number,
  poster_path: string | null,
  production_companies: IProductionCompany[],
  production_countries: IProductionCountry[],
  release_date: string,
  revenue: number,
  runtime: number | null,
  spoken_languages: ISpokenLanguage[],
  status: string,
  tagline: string | null,
  title: string,
  video: boolean,
  vote_average: number,
  vote_count: number
}

export default IMoviDetals;
export interface IGendre {
  id: number,
  name: string
}

export default IGendre;
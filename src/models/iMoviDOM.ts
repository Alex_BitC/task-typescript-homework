interface IMoviDOM {
  poster_path: string | null,
  overview: string | null,
  release_date: string,
  id: number,
  title: string,
}

export default IMoviDOM;
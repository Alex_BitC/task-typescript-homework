import IResponseMovi from './iResponseMovi';

interface IResponseAPI {
  page: number,
  results: [] | IResponseMovi[],
  dates: {
    maximum: string;
    minimum: string;
  }
  total_results: number,
  total_pages: number
}

export default IResponseAPI;